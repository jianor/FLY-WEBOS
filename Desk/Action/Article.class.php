<?php

class Article extends Action{	

	private $cacheDir='article';//缓存目录
	
	
//查询数据列表
	public function main(){
	
		empty($_GET['page'])?$currentPage=1:$currentPage= $_GET['page'];
		$page_size=2;
		$countSql='select id from tb_demo';
		$totalNum=$this->C($this->cacheDir)->countRecords($countSql);	//计算记录数		
		$page = _instance('Extend/Page',array($totalNum, $page_size, $currentPage,  ACT.'/Index/execute/page/'));//初始化框架扩展中分页类
				 	
		$sql=" select * from tb_demo  order by id desc limit ".($currentPage-1)*$page_size.",$page_size";		
		$list=$this->C($this->cacheDir)->findAll($sql);//查询结果为二维数组，需foreach循环
		
		$assignArray=array('articleList'=>$list,'page'=>$page->createPage());//适合多个变量注入	
		return $assignArray;
		
		/*
	 用户在Index类中可以调用此公共方法
     public function execute(){
		
		$article=_instance('Action.Article')->execute();//实例化Action下的Article类并调用其execute方法，返回数组值。
		$this->assign($article);
		$this->show();	
	  }
	  
	  或不作为公共方法
	  直接访问index.php/Article/execute则将上20-21行代码改为：
	  $this->assign('articleList'=>$list);
	  $this->assign('page'=>$page->createPage());
	  $this->show();
		
		*/
		 
	}
	public function noCacheArticle(){

		empty($_GET['page'])?$currentPage=1:$currentPage= $_GET['page'];
		$page_size=2;
		$countSql='select id from tb_demo';
		$totalNum=$this->C()->countRecords($countSql);	//计算记录数		
		$page = _instance('Extend/Page',array($totalNum, $page_size, $currentPage,  ACT.'/Index/execute/page/'));//初始化框架扩展中分页类
					
		$sql=" select * from tb_demo  order by id desc limit ".($currentPage-1)*$page_size.",$page_size";		
		$list=$this->C()->findAll($sql);//查询结果为二维数组，需foreach循环
		
		$assignArray=array('articleList'=>$list,'page'=>$page->createPage());//适合多个变量注入	
		return $assignArray;
	}
	
//查询一条记录	
	public function findOne(){
	
		$this->assign($this->getOne());
		$this->show('articleOne');				
	}

//添加记录		
	public function add (){
		
		if(empty($_POST)){
			$this->show('articleAdd');     		      
		}else{
			
			$createTime=date("Y-m-j",time());
			$sql="insert into tb_demo (title,content,time) values ('$_POST[title]','$_POST[content]','$createTime')";					
			if($this->C($this->cacheDir)->update($sql)){//返回新增记录ID，用户可以返回值是否大于零作判断
			$this->location('Index');
			}
		}		
	}	

//修改记录	
	public function update (){
	 
		if(empty($_POST)){

			$this->assign($this->getOne());//调用方法，并注入到页面中
			$this->show('articleUpdate');
			
		}else{		
			$sql="update tb_demo set title= '$_POST[title]',content= '$_POST[content]' where id=".$_GET['id'];		
			if($this->C($this->cacheDir)->update($sql)>=0){//返回影响记录数大于等于零表示：大于零时内容已被修改，等于零时内容没有修改但用户有提交更新操作动作
			$this->location('Index');
			}			 
		}			
	}


//删除记录
	public function del (){

		$sql="delete from tb_demo where id=".$_GET['id'];											 
		if($this->C($this->cacheDir)->update($sql)>0){
		$this->location('Index');
		}else{
		echo '删除失败';//返回影响记录数等于零表示删除失败
		}
	}
	


//查询一条记录公共方法
	public function getOne(){
	
		$sql="select * from tb_demo where id=".$_GET['id'];				 
		$articleOne= $this->C($this->cacheDir)->findOne($sql);
		$assingArray=array('articleOne'=>$articleOne);
		return $assingArray;		
				
	}	


}//

?> 

