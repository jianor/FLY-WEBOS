<?php 
class Config extends Action{	

	public function App() {
		return _instance('Action/App'); 
	}	
	
	//得到系统配置参数
	public function get_config(){
		$sql 	= "select * from config;";
		$list	= $this->db()->fetchAll($sql);
		if(is_array($list)){
			foreach($list as $key=>$row){
				$assArr[$row["filedname"]] = $row["filedvalue"];
			}
		}
		return $assArr;		
	}
	
	//系统常规设置
	public function set_config(){
		if(empty($_POST)){
			$config		= $this->get_config();
			$dock		= $this->App()->app_checked("dock",$config["dock"]);
			$desk1		= $this->App()->app_checked("desk1",$config["desk1"]);
			$web		= $this->get_lighttpd("www");	
			$network	= $this->get_network();	
			$dns		= $this->get_dns();
			$ifname		= $this->get_ifname();
			$assArr		=array(
							'one'=>$config,'dock'=>$dock,'desk1'=>$desk1,'web'=>$web,
							'dns'=>$dns,'ifnamelist'=>$ifname
						);
			$this->assign($assArr);
			$this->show('config');					
		}else{
			foreach($_POST as $key=>$v){
				if( is_array($v) ) $v=implode(",",$v);
				$sql="update config set filedvalue='$v' where filedname='$key'";
				$this->db()->query($sql);
			}
			
			$srv_port=$this->_REQUEST("srv_port");
			$sql ="update lighttpd set srv_port='$srv_port' where name='www'";
			$this->db()->query($sql);
		}
	}


	//网络常规设置

	public function get_ifname(){
		$sql 		= "select * from phynic";
		$list		= $this->db()->fetchAll($sql);	
		return $list;		
	}

	public function get_network(){
		$sql 		= "select * from network";
		$list		= $this->db()->fetchAll($sql);	
		return $list;	
	}
	public function get_ifname_network(){
		$ifname		= $this->_REQUEST("ifname");
		$sql 		= "select * from network where ifname='$ifname'";
		$list		= $this->db()->fetchOne($sql);	
		echo json_encode($list);				
	}
	
	public function set_network(){
		$ifname		=$this->_REQUEST("ifname");
		$ipaddr		=$this->_REQUEST("ipaddr");
		$subnet		=$this->_REQUEST("subnet");
		$gateway	=$this->_REQUEST("gateway");
		$id			=time();
		$result=$this->db()->fetchOne("select count(*) as number from network where ifname='$ifname';");
		if($result["number"]>0){
			$sql ="update network set name='$ifname',ifname='$ifname',ipaddr='$ipaddr',subnet='$subnet',gateway='$gateway',type='STATIC' where ifname='$ifname';";
		}else{
			$sql ="insert into network('id','name','ifname','ipaddr','subnet','gateway','type') 
					values('$id','$ifname','$ifname','$ipaddr','$subnet','$gateway','STATIC')";
		}
		echo $sql;
		$this->db()->query($sql);
	}	
	
	public function get_lighttpd($name){
		$sql 	= "select * from lighttpd where name='$name'";
		$row	= $this->db()->fetchOne($sql);
		return $row;
	}
	
	//系统DNS
	
	public function get_dns(){
		$sql 	= "select * from resolv";
		$row	= $this->db()->fetchOne($sql);
		return $row;			
	}
	
	public function set_dns(){
		$dns1=$this->_REQUEST("dns1");
		$dns2=$this->_REQUEST("dns2");
		$dns3=$this->_REQUEST("dns3");
		$result=$this->db()->fetchOne("select count(*) as number from resolv");
		if($result["number"]>0){
			$sql ="update resolv set dns1='$dns1',dns2='$dns2',dns3='$dns3'";
		}else{
			$sql ="insert into resolv('dns1','dns2','dns3') values('$dns1','$dns2','$dns3')";
		}
		$this->db()->query($sql);
	}	
	
}

?>
