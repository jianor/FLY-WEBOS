<?php
class Index extends Action{
	public function __construct() {
		 $this->checklogin();
		 //_instance('Action/Auth'); 
	}	
	public function Config() {
		return _instance('Action/Config'); 
	}		
	public function main(){
		$sql = "select url from wallpaper where id='".$_SESSION["deskos"]["m_wallpaper_id"]."'";
		$one = $this->db()->fetchOne($sql);
		$cnf = $this->L("Config")->get_config();
		$this->assign(array('one'=>$one,'cnf'=>$cnf));
		$this->show('index');	
	}
	
	//得到背景图片
	public function wallinit(){
		$sql="select url from wallpaper where id='".$_SESSION["deskos"]["m_wallpaper_id"]."'";
		$one=$this->db()->fetchOne($sql);
		echo $one["url"];	
	}

	public function wallpaper(){
		$sql	="select id,url from wallpaper limit 0,12";
		$list	=$this->db()->fetchAll($sql);
		$txt	="<table width='100%' cellpadding='5'><tr>";			
		if(is_array($list)){
			foreach($list as $key=>$row){
				$txt .="<td>";
				$txt .="<a href=javascript:wallpaper_set('".$row["url"]."','".$row["id"]."')><img src=".APP."/View/".$row["url"]." width='150' height='120'></a>";
				$txt .="</td>";
				if(($key+1)%4 == 0) $txt.="</tr>";
			}
		}	
		$txt .="</table>";
		echo $txt;
	}

	//删除桌面上的图标
	public function wallpaper_set(){
		$id	= $this->_REQUEST("id");
		$sql 	= "update manager set wallpaper_id='$id' where username='".$_SESSION["deskos"]["username"]."';";
		if($this->db()->query($sql)){
			$_SESSION["deskos"]["m_wallpaper_id"]=$id;
		}
	}	
	
	//用户登录
	public function login(){
		if(empty($_POST)){
			$this->show('login');	
		}else{
			if($this->login_check()){
				$this->show('index');	
			}else{
				$this->assign(array('txt'=>"<font color=red>你的登录信息有问题</a>"));
				$this->show('login');	
			}	
		}	
	}
	
	//锁定背景
	public function lock_login(){
		if($this->login_check()){
			echo "1";
		}else{
			echo "0";
		}	
	}
	
	
	public function login_check(){
		$username = ($_POST["username"])?$_POST["username"]:$_GET["username"];
		$password = ($_POST["password"])?$_POST["password"]:$_GET["password"];
		$row	  = $this->db()->fetchOne("select * from manager where username = '$username' and password='$password'");
		if(!empty($row)){
			$_SESSION["deskos"]["username"]=$username;
			$_SESSION["deskos"]["password"]=$password;
			$_SESSION["deskos"]["m_wallpaper_id"]=$row["wallpaper_id"];
			//$_SESSION["deskos"]["dock"] =$row["dock"];
			$_SESSION["deskos"]["desk1"]=$row["desk1"];
			$_SESSION["deskos"]["desk2"]=$row["desk2"];
			$_SESSION["deskos"]["desk3"]=$row["desk3"];
			$_SESSION["deskos"]["desk4"]=$row["desk4"];
			$_SESSION["deskos"]["desk5"]=$row["desk5"];	
			$_SESSION["deskos"]["ischecklogin"]=1;
			if(empty($_SESSION["deskos"]["desk1"])){
				$_SESSION["deskos"]["desk1"]=$this->L("Role")->role_get_appid($row["role"]);
			}
			return true;
		}else{
			return false;
		}
	}
	
	public function login_lock(){
		$assArr = array("username"=>$_SESSION["deskos"]["username"],"password"=>$_SESSION["deskos"]["password"],);
		echo json_encode($assArr);
	}
	
	
	
	public function logout(){
		unset($_SESSION["deskos"]);
		print_r($_SESSION["deskos"]);
	}
	
	
	
	//得到所有应用管理
	public function sapp(){
		$sApp= $this->db()->fetchAll("select * from app",'appid');
		 echo json_encode($sApp);
	}

	//得桌面图标
	public function deskapp(){
		$num=($_POST["num"])?$_POST["num"]:$_GET["num"];
		echo $_SESSION["deskos"][$num];	
	}
	
	//删除桌面上的图标
	public function deskappdel(){
		$desknum	= $this->_REQUEST("deskNum");
		$appid		= $this->_REQUEST("appid");
		$deskfiled  ="desk".$desknum;
		$arr1	= explode(",",$_SESSION["deskos"][$deskfiled]);
		unset($arr1[array_search($appid,$arr1)]);
		$diff	=implode(",",$arr1);
//		$arr2	= explode(",",$appid);
//		$diff	= implode(",",array_diff_key($arr1,$arr2));
		$sql 	= "update manager set $deskfiled='$diff' where username='".$_SESSION["deskos"]["username"]."';";
		if($this->db()->query($sql)){
			$_SESSION["deskos"][$deskfiled]=$diff;	
		}
	}

	//判断是登录过，如果没有就初始化值
	public function  checklogin(){
		if(empty($_SESSION["deskos"]["ischecklogin"])){
				$config = $this->Config()->get_config();
				$_SESSION["deskos"]["m_wallpaper_id"]=17;
				$_SESSION["deskos"]["dock"] =$config["dock"];
				$_SESSION["deskos"]["desk1"]=$config["desk1"];
				$_SESSION["deskos"]["desk2"]="";
				$_SESSION["deskos"]["desk3"]="";
				$_SESSION["deskos"]["desk4"]="";
				$_SESSION["deskos"]["desk5"]="";	
		}
	}	


//this=>assign方法有如下方式

//$this->assign('A',$A);
//$this->assign(array('A'=>$A,'B'=>$B,'E'=>$E,'F'=>$F));
//$this->assign(array('A'=>$A,'B'=>$B),array('E'=>$E,'F'=>$F));
	public function cacheTest(){
	
		$article=_instance('Action/Article')->noCacheArticle();
		//实例化Action下的Article类并调用其execute方法，返回数组值。
		$this->assign($article);
		$this->show('index');	
		//参数留空则默认访问View下的index.php,若在View的子目下，可以使用$this->show('test/index'); 
	}
 
}//

?>
