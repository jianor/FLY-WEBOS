<?php 

class Manager extends Action{
	public function __construct() {
//		 $struct=require(EXTEND . 'Struct.php');
//		 print_r($struct);
//		 $this->db->refreshStruct($struct);
		$auth=_instance('Action/Auth');
		 
	}	
	public function Role() {
		return _instance('Action/Role'); 
	}	
		
	//得到所有应用管理
	public function manager(){
	
		empty($_GET['page'])?$currentPage=1:$currentPage= $_GET['page'];
		$page_size=10;
		$countSql="select * from manager";
		$totalNum=$this->db()->countRecords($countSql);	//计算记录数
		$page = _instance('Extend/Page',array($totalNum, $page_size, $currentPage,  ACT.'/Manager/manager_show/page/'));	
		$sql=" select * from manager  order by id desc limit ".($currentPage-1)*$page_size.",$page_size";		
		$list=$this->db()->fetchAll($sql);//查询结果为二维数组，需foreach循环
		if(is_array($list)){
			foreach($list as $key=>$row){
				$role[$row['id']] = $this->Role()->role_get_name($row['role']);
			}
		}
		$assignArray=array('list'=>$list,'page'=>$page->createPage(),"role"=>$role);//适合多个变量注入	
		return $assignArray;
		
	}

	public function manager_show(){
		$list=$this->manager();
		$this->assign($list);
		$this->show('manager');	
	}
	
	public function manager_add(){
		if(empty($_POST)){
			$role = $this->Role()->role_checked("role");
			$this->assign(array("role"=>$role));
			$this->show('manager_add');	
		}else{
			$role  = ($_POST["role"])?$_POST["role"]:$_GET["role"];
			$role  = implode(",",$role);
			$maxid	= date("YmdHis").rand(1000,9999);
			$now	= date("Y-m-d",time());
			$sql	= "insert into manager(id,username,password,role,regdt)
									 values('$maxid','$_POST[username]','$_POST[password]','$role','$now');";
			if($this->db()->query($sql)){
				$this->location('操作成功','Manager/manager_show');	
			}
		}
		
	}
	public function manager_del(){
		$id  = ($_POST["id"])?$_POST["id"]:$_GET["id"];
		$sql	= "delete from manager where id='$id'";
		if($this->db()->query($sql)){
			$this->location('操作成功','Manager/manager_show');	
		}
	} 

	public function manager_modify(){
		if(empty($_POST)){
			$sql  = "select * from manager where id='".$_GET["id"]."'";
			$one  = $this->db()->fetchOne($sql);
			$role = $this->Role()->role_checked("role",$one["role"]);
			$this->assign(array("one"=>$one,"role"=>$role));
			$this->show('manager_modify');	
		}else{
			$role  = ($_POST["role"])?$_POST["role"]:$_GET["role"];
			$role  = implode(",",$role);
			$id    = ($_POST["id"])?$_POST["id"]:$_GET["id"];
			$sql   = "update manager set   password='$_POST[password]', 
										role='$role'
								 where  id='$id';";
			if($this->db()->query($sql)){
				$this->location('操作成功','Manager/manager_show');	
			}
		}
	}

 
}//

?>