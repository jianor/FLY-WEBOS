<?php 
class Role extends Action{
	public function __construct() {

//		 $struct=require(EXTEND . 'Struct.php');
//		 print_r($struct);
//		 $this->db->refreshStruct($struct);
		$auth=_instance('Action/Auth');
		 
	}		
	public function App() {
		return _instance('Action/App'); 
	}		
	//得到所有权限
	public function role(){
		empty($_GET['page'])?$currentPage=1:$currentPage= $_GET['page'];
		$page_size=5;
		$countSql="select * from role";
		$totalNum=$this->db()->countRecords($countSql);	//计算记录数
		$page = _instance('Extend/Page',array($totalNum, $page_size, $currentPage,  ACT.'/Role/role_show/page/'));	
		$sql=" select * from role  order by id desc limit ".($currentPage-1)*$page_size.",$page_size";		
		$list=$this->db()->fetchAll($sql);//查询结果为二维数组，需foreach循环
		if(is_array($list)){
			foreach($list as $key=>$row){
				$app[$row['id']] = $this->App()->app_get_name($row['appid']);
			}
		}
		$assignArray=array('list'=>$list,'page'=>$page->createPage(),'app'=>$app);//适合多个变量注入	
		return $assignArray;
		
	}

	public function role_show(){
		$list=$this->role();
		$this->assign($list);
		$this->show('role');	
	}
	public function role_add(){
		if(empty($_POST)){
			$app = $this->App()->app_checked("appid");
			$this->assign(array("app"=>$app));
			$this->show('role_add');	
		}else{
			$appid  = ($_POST["appid"])?$_POST["appid"]:$_GET["appid"];
			$appid  = implode(",",$appid);
			$maxid		= date("YmdHis").rand(1000,9999);
			echo $id;
			$sql	= "insert into role(id,name,desc,appid) values('$maxid','$_POST[name]','$_POST[desc]','$appid');";
			if($this->db()->query($sql)){
			
			}
			$this->location('操作成功','Role/role_show');	
		}
		
	}
	public function role_del(){
			$id  = ($_POST["id"])?$_POST["id"]:$_GET["id"];
			$sql	= "delete from role where id='$id'";
			if($this->db()->query($sql)){
			
			}
			$this->location('操作成功','Role/role_show');	
	} 

	public function role_modify(){
		if(empty($_POST)){
			$sql = "select * from role where id='".$_GET["id"]."'";
			$one = $this->db()->fetchOne($sql);
			$app = $this->App()->app_checked("appid",$one["appid"]);
			$this->assign(array("one"=>$one,"app"=>$app));
			$this->show('role_modify');	
		}else{
			$appid  = ($_POST["appid"])?$_POST["appid"]:$_GET["appid"];
			$appid  = implode(",",$appid);
			$id     = ($_POST["id"])?$_POST["id"]:$_GET["id"];
			$sql	= "update role set   name='$_POST[name]', desc='$_POST[desc]', appid='$appid' where id='$id';";

			if($this->db()->query($sql)){
			
			}
			$this->location('操作成功','Role/role_show');	
		}
	}
	
 	/**
    * 获得所有的查询数据，并且通选择框形式返回
    * @access function
    * @param string $inputname  选择框名字
    * @param int $id  默认选择中
    * @return string 选择框
    */
	public function role_checked($inputname,$id=null){
		$id_arr = (!is_array($id))?explode(",",$id):0;
		$sql	=" select * from role order by id desc";		
		$list	=$this->db()->fetchAll($sql);//查询结果为二维数组，需foreach循环
		$string = "";
		if(is_array($list)){
			foreach($list as $key=>$row){
				$string .="<li style='list-style:none;width:150px;float:left;'><input type='checkbox' name='".$inputname."[]' value='".$row["id"]."' " ;
				if(in_array($row["id"],$id_arr)) $string .=" checked";
				$string .="> ".$row["name"];
				$string .= "</li>";
			}
		}
		return $string;
	}
	/*********************************************************************
	 * 根据传入的ID值查询出相对的名称
	 * ex:$id = 1,3,5, 
	*/
	
	public function role_get_name($id){
		$sql  ="select name from role where id in ($id)";	
		$list =$this->db()->fetchAll($sql);//查询结果为二维数组，需foreach循环
		$str  ="";
		if(is_array($list)){
			foreach($list as $row){
				$str .= "|-".$row["name"]."&nbsp;";
			}
		}
		return $str;
	}

	/*********************************************************************
	 * 根据传入的ID值查询出相对的管理Appid
	 * ex:$id = 1,3,5, 
	*/	
	public function role_get_appid($id){
		$sql  	="select appid from role where id in ($id)";	
		$list 	=$this->db()->fetchAll($sql);//查询结果为二维数组，需foreach循环
		$rtnArr	= array();
		if(is_array($list)){
			foreach($list as $row){
				$rtnArr[] = $row["appid"];
			}
		}
		$rtnStr=implode(",",$rtnArr);
		$rtnArr=array_unique(explode(",",$rtnStr));
		return implode(",",$rtnArr);	
	}

}//
?>
