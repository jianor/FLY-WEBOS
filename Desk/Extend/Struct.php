<?php
//数据库表和字段的定义
return  array(
  //APP 存储应用的列表
  "app" => array(
  		  "appid"          =>  "INT(16) PRIMARY KEY",
          "name"  =>  "VARCHAR(255)", //图标名称
          "icon"  =>  "tinytext",	//图标图片
          "url"     =>  "tinytext",//图标链接
          "type"     =>  "VARCHAR(255)",//应用类型，参数有：app ,sapp
		  "kindid"     =>  "int(11)",
		  "width"     =>  "int(11)",//窗口宽度
		  "height"     =>  "int(11)",//窗口高度
		  "isresize"     =>  "tinytext(1)", //是否能对窗口进行拉伸
		  "isopenmax"     =>  "tinytext(1)", //是否打开直接最大化
		  "isflash"     =>  "tinytext(1)", //是否为flash应用
		  "remark"     =>  "tinytext",
		  "indexid"     =>  "BIGINT(20) NULL DEFAULT '1'",
		  "dt"     =>  " DATETIME NULL DEFAULT NULL",
		  "asc"     =>  "int(11)"
      ),
	//wallpaper 桌面图标列表
	"wallpaper" => array(
		  "id"          =>  "INT(16) PRIMARY KEY",
		  "title"  =>  "VARCHAR(255)", //图标名称
		  "url"     =>  "tinytext",//图标链接
		  "width"     =>  "int(11)",//窗口宽度
		  "height"     =>  "int(11)",//窗口高度
	 ),
	//manager 桌面图标列表
	"manager" => array(
		"id"        =>  "INT(16) PRIMARY KEY",
		"username"  =>  "VARCHAR(255)", 
		"password"  =>  "VARCHAR(255)", 
		"role" 		=>  "tinytext",//权限字符串
		"dock"  	=> "tinytext DEFAULT ''",
		"desk1"		=> "tinytext DEFAULT ''" , 
		"desk2"		=> "tinytext DEFAULT ''" , 
		"desk3"		=> "tinytext DEFAULT ''" , 
		"desk4"		=> "tinytext DEFAULT ''" , 
		"desk5"		=> "tinytext DEFAULT ''" , 
		"appxy"		=> "VARCHAR(255)" , 
		"dockpos"	=> "VARCHAR(255) NULL DEFAULT 'left'", 
		"wallpaper_id"=>"INT(11) NULL DEFAULT '1'", 
		"wallpaperwebsite"=>"TEXT NULL",
		"wallpaperstate" => "TINYINT(4) NULL", 
		"wallpapertype"=> "VARCHAR(255) NULL" , 
		"skin"=> "VARCHAR(255) NULL DEFAULT 'default'", 
		"regdt" => "DATETIME", 
		"lastlogindt"=> "DATETIME", 
		"lastloginip"=> "VARCHAR(255)",
		"remark"=> "VARCHAR(255)"
	 ),	 
 	//manager 桌面图标列表
	"role" => array(
		"id"        =>  "VARCHAR(255) PRIMARY KEY",
		"name"  =>  "VARCHAR(255)", 
		"desc"  =>  "VARCHAR(255)", 
		"appid" =>  "VARCHAR(255)",//权限字符串
	 ),
	 "config"=>array(
		"filedname"  =>  "VARCHAR(255)", 
		"filedvalue" =>  "VARCHAR(255)", 
	 ),
	 "lighttpd"=>array(
	 	"name"  =>  "VARCHAR(255)", 
		"conf_path"  =>  "VARCHAR(255)", 
		"www_path" =>  "VARCHAR(255)", 
		"errlog_path" =>  "VARCHAR(255)", 
		"acclog_path" =>  "VARCHAR(255)", 
		"srv_port" =>  "VARCHAR(255)"	
	 ),

	 "mysqld"=>array(
		"data_path"  =>  "VARCHAR(255)", 
		"sock_path" =>  "VARCHAR(255)", 
		"link_enable" =>  "VARCHAR(255)", 
		"errlog_path" =>  "VARCHAR(255)", 
		"pid_path" =>  "VARCHAR(255)"	
	 ), 
);
?>