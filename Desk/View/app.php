	<?php include("header.php"); ?>
	<body>
	
		<div class="widget-box">
				<div class="widget-title">
					<span class="icon">
						<i class="icon-th"></i>
					</span>
					<h5>应用列表</h5>
				</div>
				<div class="widget-content nopadding">
					<table width="100%" class="table table-bordered table-striped">
						
						<tbody>
							<?php foreach ($list as $key=>$v) { ?>	
							<tr>
								<td width="60" align="center" valign="middle"><img src="<?php echo APP."/View/".$v["icon"];?>" width="60" height="60" class="appimg"></td>
								<td><?php echo $v["name"]?>
								<br>
								<span class="f12"><?php echo utf_substr($v["remark"],100)?></span></td>
								<td>
								<a href="<?php echo ACT?>/App/app_modify/appid/<?php echo $v["appid"]?>">
								 <button class="btn btn-primary"><i class="icon-pencil icon-white"></i> Edit</button>
								 </a>
<!--								<a href="#" onClick="javascript:app_install(<?php echo $v["appid"]?>)">
								<button class="btn"><i class="icon-eye-open"></i> View</button>
								 </a>--></td>
							</tr>
							
							<?php }?>	
						</tbody>
				  </table>							
				</div>
		</div>
	</body>
	
<script>
 function trim(str){ //删除左右两端的空格
     return str.replace(/(^\s*)|(\s*$)/g, "");
 }
 function ltrim(str){ //删除左边的空格
     return str.replace(/(^\s*)/g,"");
 }
 function rtrim(str){ //删除右边的空格
     return str.replace(/(\s*$)/g,"");
 }
function app_install(appid){
	$.ajax({
		type:'post',//可选
		url:'<?php echo ACT ?>/App/app_install/appid/'+appid,//这里是接收数据的PHP程序
		data:'',//传给PHP的数据，多个参数用&连接
		dataType:'text',
        cache: false,        
		async: false,		
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
				n=trim(msg);
				if(n=0){
					  // var iframe = this.iframe.contentWindow;
          			   //iframe.document.getElementById("btnsave").click();
					  
				}
			},
		});	
		
		
}
</script>
</html>
