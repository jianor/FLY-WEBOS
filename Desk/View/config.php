<?php include("header_art.php"); ?>
<body>
<div class="widget-box">
  <div class="widget-title">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#tab1">系统设置</a></li>
      <li><a data-toggle="tab" href="#tab3">I P配置</a></li>
      <li><a data-toggle="tab" href="#tab2">DNS配置</a></li>
    </ul>
  </div>
  <form action="<?php echo ACT?>/Config/set_config" method="post" name="myform" class="form-horizontal" />
  
  <div class="widget-content tab-content">
    <div id="tab1" class="tab-pane active">
      <div class="widget-content nopadding">
        <div class="control-group">
          <label class="control-label">桌面标题</label>
          <div class="controls">
            <input type="text" name="website" id="website" value="<?php echo $one["desktitle"]?>"/>
            <span class="help-block">设置桌面系统标题</span> </div>
        </div>
        <div class="control-group">
          <label class="control-label">WEB 端口</label>
          <div class="controls">
            <input type="text" name="srv_port" id="srv_port"  value="<?php echo $web["srv_port"]?>"   onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" style="width:50px"/>
          </div>
        </div>
        <div class="control-group">
          <label class="control-label">默认桌面应用</label>
          <div class="controls">
            <?php echo $desk1?>
          </div>
        </div>
        <div class="control-group">
          <label class="control-label">描述</label>
          <div class="controls">
            <textarea name="remark" id="remark" ><?php echo $one["remark"]?></textarea>
          </div>
        </div>
        <div class="form-actions">
          <button type="button" id="config_btn" class="btn btn-primary">保存</button>
          <button type="button" class="btn btn-primary" onClick="javascript:window.history.go(-1);">返回</button>
        </div>
      </div>
    </div>
    <div id="tab2" class="tab-pane">
      <div class="widget-content nopadding">
        <div class="control-group">
          <label class="control-label">DNS1地址</label>
          <div class="controls">
            <input type="text" name="dns1" id="dns1" value="<?php echo $dns["dns1"]?>"/>
          </div>
          <label class="control-label">DNS2地址</label>
          <div class="controls">
            <input type="text" name="dns2" id="dns2" value="<?php echo $dns["dns2"]?>"/>
          </div>
          <label class="control-label">DNS3地址</label>
          <div class="controls">
            <input type="text" name="dns3" id="dns3" value="<?php echo $dns["dns3"]?>"/>
          </div>
        </div>
        </div>
        <div class="form-actions">
          <button type="button" id="dns_btn" class="btn btn-primary">保存</button>
          <button type="button" class="btn btn-primary" onClick="javascript:window.history.go(-1);">返回</button>
        </div>
      </div>
    <div id="tab3" class="tab-pane">
      <div class="widget-content nopadding">
        <div class="control-group">
        <label class="control-label">网络接口</label>
        <div class="controls">
               <select id="ifname" name="ifname">
                <?php foreach ($ifnamelist as $key=>$v) { ?>	
                <option value="<?php echo $v["logic_name"] ?>"><?php echo $v["logic_name"] ?></option>
               <?php }?>	
              </select> 
          </div>  
          <label class="control-label">I P 地址</label>
          <div class="controls">
            <input type="text" name="ipaddr" id="ipaddr" />
          </div>
          <label class="control-label">掩码地址</label>
          <div class="controls">
            <input type="text" name="subnet" id="subnet"/>
          </div>
          <label class="control-label">网关地址</label>
          <div class="controls">
            <input type="text" name="gateway" id="gateway" />
          </div>
        </div>
        </div>
        <div class="form-actions">
          <button type="button" id="ipaddr_btn" class="btn btn-primary">保存</button>
          <button type="button" class="btn btn-primary" onClick="javascript:window.history.go(-1);">返回</button>
        </div>
      </div>
  </div>
  </form>
</div>
</body>
</html>
<script  type="text/javascript">

function onLoadNetwork(ifname){
	$.ajax({
		type:'post',//可选
		url:'<?php echo ACT ?>/Config/get_ifname_network/ifname/'+ifname,//这里是接收数据的PHP程序
		dataType:'text',
		cache: false,        
		async: false,		
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
				var obj = eval('(' + msg + ')');
				$('#ipaddr').val(obj.ipaddr)
				$('#subnet').val(obj.subnet)
				$('#gateway').val(obj.gateway)
			}
		});	
}

$(document).ready(function(){

  //载入默认的IP信息
  ifname = $("#ifname").val();
  onLoadNetwork(ifname)

  $("#ifname").change(function(){
	  ifname = $("#ifname").val();
	  onLoadNetwork(ifname)
  });
	
	
  $("#config_btn").click(function(){
	website		=$("#website").val();
	srv_port 	=$("#srv_port").val();
	remark		=$("#remark").val();
	var desk1="";     
//	$("input[name='desk1[]'][checked]").each(function(){     
//	 desk1+=$(this).val()+",";     
//	//alert($(this).val());     
//	}) 
	$("input[name='desk1[]").each(function(){
		if ($(this).is(':checked')) {
			desk1+=$(this).val()+",";      
		} 
	});
	$.ajax({
		type:'post',//可选
		url:'<?php echo ACT ?>/Config/set_config/',//这里是接收数据的PHP程序
		data:{
			website:website,	
			srv_port:srv_port,
			remark:remark,
			desk1:desk1,
		},//传给PHP的数据，多个参数用&连接
		dataType:'text',
		cache: false,        
		async: false,		
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
				art.dialog({
					content: '保存成功',
					icon: 'succeed',
					fixed: false,
					lock: true,
					max:false,
					min:false,
					time: 1.5
				});
			}
		});	
  });
  $("#dns_btn").click(function(){
	dns1	=$("#dns1").val();
	dns2 	=$("#dns2").val();
	dns3	=$("#dns3").val();
	$.ajax({
		type:'post',//可选
		url:'<?php echo ACT ?>/Config/set_dns/',//这里是接收数据的PHP程序
		data:{
			dns1:dns1,	
			dns2:dns2,
			dns3:dns3,
		},//传给PHP的数据，多个参数用&连接
		dataType:'text',
		cache: false,        
		async: false,		
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
				art.dialog({
					content: '保存成功',
					icon: 'succeed',
					fixed: false,
					lock: true,
					max:false,
					min:false,
					time: 1.5
				});
			}
		});	
  }); 
 
  $("#ipaddr_btn").click(function(){
	ifname	=$("#ifname").val();
	ipaddr 	=$("#ipaddr").val();
	subnet	=$("#subnet").val();
	gateway	=$("#gateway").val();
	$.ajax({
		type:'post',//可选
		url:'<?php echo ACT ?>/Config/set_network/',//这里是接收数据的PHP程序
		data:{
			ifname:ifname,	
			ipaddr:ipaddr,
			subnet:subnet,
			gateway:gateway,
		},//传给PHP的数据，多个参数用&连接
		dataType:'text',
		cache: false,        
		async: false,		
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
				art.dialog({
					content: '保存成功',
					icon: 'succeed',
					fixed: false,
					lock: true,
					max:false,
					min:false,
					time: 1.5
				});
			}
		});	
  });


  
});
</script>