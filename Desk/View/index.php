﻿<!DOCTYPE html >
<html>
	<head>
		<title><?php echo $cnf["desktitle"] ?></title>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
		<link href="<?php echo APP;?>/View/css/main.css" rel="stylesheet" type="text/css"/>
		<link href="<?php echo APP;?>/View/css/themesetting.css" rel="stylesheet"  type="text/css" >
		<link href="<?php echo APP;?>/View/css/skins/black.css" rel="stylesheet" />
		<link href="<?php echo APP;?>/View/css/powerFloat.css" rel="stylesheet" type="text/css"/>	
		<link href="<?php echo APP;?>/View/css/smartMenu.css" rel="stylesheet"  type="text/css" />
		
	</head>
	<body>
		
		<div id="zoomWallpaperGrid" class="zoomWallpaperGrid" style="position: absolute; z-index: -10; left: 0pt; top: 0pt; overflow: hidden; height: 381px; width: 1440px;">
			<img id="zoomWallpaper" class="zoomWallpaper" style="position: absolute; top: 0pt; left: 0pt; height: 381px; width: 1440px;" src="<?php echo APP;?>/View/<?php echo $one["url"];?>">
		</div>
		<div class="taskbar_start_menu_container" id="startMenuContainer" _olddisplay="block" style="display: none;">  
			<div class="startMenuImg taskbar_start_menu_body" id="taskbar_start_menu_body">          
			<div uin="0" class="taskbar_start_menu_selfinfo" id="startMenuSelfInfo">       
				<div class="taskbar_start_menu_nick" id="startMenuSelfNick">请&nbsp;<a href="###">登录</a></div>    
				<a title="反馈" href="###" class="startMenuImg startMenuTopControl_support" cmd="support">&nbsp;</a>    
				<a title="锁定" href="###" class="startMenuImg startMenuTopControl_lock" cmd="lock">&nbsp;</a>     
			</div>              
			<ul class="taskbar_start_menu">
				<li cmd="favorite">
					<a title="添加到收藏夹" href="###">添加到收藏夹</a>
				</li>
				<li cmd="shortcut">
				<a title="保存桌面快捷方式" target="_blank" href="###">保存桌面快捷方式</a></li>
				<li cmd="download">
					<a title="下载客户端" href="###">下载客户端</a></li>
					<li title="关于Q+ Web" cmd="about" id="taskbar_helpButton">
					<a href="###">关于Q+ Web</a>
				</li>
				<li cmd="helper">
					<a title="新手指导" href="###">新手指导</a>
				</li></ul>    
				<a class="startMenuImg logout_botton" title="注销当前用户" cmd="logout" href="###"></a>
				</div>
		</div>	
	</body>
</html>
<script type="text/javascript">
 function trim(str){ //删除左右两端的空格
	 return str.replace(/(^\s*)|(\s*$)/g, "");
 }
 function ltrim(str){ //删除左边的空格
	 return str.replace(/(^\s*)/g,"");
 }
 function rtrim(str){ //删除右边的空格
	 return str.replace(/(\s*$)/g,"");
 }
function UIPATH(){
	var UIPATH="<?php echo APP ?>/View/";
	return UIPATH;
}
function ACTPATH(){
	var ACTPATH="<?php echo ACT ?>";
	return ACTPATH;
}

function sAppData(){
	$.ajax({
		type:'post',//可选
		url:'<?php echo ACT ?>/Index/sapp',//这里是接收数据的PHP程序
		data:'ac=sapp',//传给PHP的数据，多个参数用&连接
		dataType:'text',
		cache: false,        
		async: false,		
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
				sAppData = eval('(' + trim(msg) + ')');
			}
		});	
	return sAppData;
}

//return string ex: qq,jingshang,
function deskIconData(desknum){
	$.ajax({
		type:'post',//可选
		url:'<?php echo ACT ?>/Index/deskapp',//这里是接收数据的PHP程序
		data:'num='+desknum,//传给PHP的数据，多个参数用&连接
		dataType:'text',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
		cache: false,        
		async: false,		
		success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
			txtStr=trim(msg);		
		},
		error: function(){
			 txtStr=false;
		}
	});	
	return txtStr;
}
</script>
<script  type="text/javascript" src="<?php echo APP;?>/View/js/jquery-1.7.1.min.js"></script>
<script  type="text/javascript" src="<?php echo APP;?>/View/js/jquery-powerFloat-min.js"></script>
<script  type="text/javascript" src="<?php echo APP;?>/View/js/jquery-smartMenu-min.js"></script>
<script  type="text/javascript" src="<?php echo APP;?>/View/js/jquery-class.js"></script>
<script  type="text/javascript" src="<?php echo APP;?>/View/js/artDialog.js"></script>
<script  type="text/javascript" src="<?php echo APP;?>/View/js/iframeTools.js"></script>
<script  type="text/javascript" src="<?php echo APP;?>/View/js/jquery.ui.core-min.js"></script>
<script  type="text/javascript" src="<?php echo APP;?>/View/js/jquery.ui.widget-min.js"></script>
<script  type="text/javascript" src="<?php echo APP;?>/View/js/jquery.ui.mouse-min.js"></script>
<script  type="text/javascript" src="<?php echo APP;?>/View/js/jquery.ui.draggable-min.js"></script>
<script  type="text/javascript" src="<?php echo APP;?>/View/js/jquery.ui.droppable-min.js"></script>
<script  type="text/javascript" src="<?php echo APP;?>/View/js/jquery.ui.sortable.js"></script>
<script  type="text/javascript" src="<?php echo APP;?>/View/js/webos-core.js"></script>
<script  type="text/javascript" src="<?php echo APP;?>/View/js/ajax.js"></script>
<script  type="text/javascript">
$.ajax({
	type:'post',//可选
	url:'<?php echo ACT ?>/Index/wallinit/',//这里是接收数据的PHP程序
	data:'ac=wallpaper_init',//传给PHP的数据，多个参数用&连接
	dataType:'text',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
	cache: false,        
	async: false,		
	success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
			var src="<?php echo APP ?>/View/"+trim(msg);
			var h = $(window).height();
			var w = $(window).width();
			$("#zoomWallpaper").attr("src",src).width(w).height(h);
			$("#zoomWallpaperGrid").width(w).height(h);
	}
});

</script>