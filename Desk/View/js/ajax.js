﻿//var UIPATH=UIPATH();
//alert(UIPATH);
var ACTPATH=ACTPATH();


//创建桌面最外层类
deskAjaxIcon=function(me){
	return me={	
		del:function(appid,deskNum){
			postData={
				deskNum:deskNum,
				appid:appid
			}
			$.ajax({
				type:'post',//可选
				url:ACTPATH+'/Index/deskappdel',//这里是接收数据的PHP程序
				data:postData,//传给PHP的数据，多个参数用&连接
				dataType:'text',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
				cache: false,        
				async: false,		
				success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
					Deskpanel.refresh();
				}
			});			
		}, 
		install:function(appid,deskNum){
			postData={
				deskNum:deskNum,
				appid:appid
			}
			$.ajax({
				type:'post',//可选
				url:ACTPATH+'/Index/deskappinstall',//这里是接收数据的PHP程序
				data:postData,//传给PHP的数据，多个参数用&连接
				dataType:'text',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
				cache: false,        
				async: false,		
				success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
					Deskpanel.refresh();
				}
			});			
		}, 
		create:function(){		
			me.box=$("<div id='desktop'  style='position: static;'></div>");
			Body.addPanel(me.box);
		},
		addPanel:function(panel){
			me.box.append(panel);
		},
		show:function(){
			me.box.show();
		},
		hide:function(){
			me.box.hide();
		}
	};
}();



//创建桌面最外层类
loginAjax=function(me){
	return me={	
		lock:function(){
			$.ajax({
				type:'post',//可选
				url:ACTPATH+'/Index/logout',//这里是接收数据的PHP程序
				data:'',//传给PHP的数据，多个参数用&连接
				dataType:'text',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
				cache: false,        
				async: false,		
				success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
				
				}
			});
			var dialog = art.dialog({
				lock: true,
				title:'系统登录',
				content: ''
					+ '<br>帐号：<input id="username" style=" padding:4px" /><br>'
					+ '<br>密码：<input id="password" style=" padding:4px" /><br>',
				fixed: true,
				resize:false,
				max:false,
				min:false,
				id: 'Fm7',
				icon: 'face-smile',
				okVal: '开始登录',
				ok: function () {
					var username = $("#username").val();
					var password = $("#password").val();
					ajaxData={
							username:username,
							password:password
						}
					$.ajax({
						type:'post',//可选
						url:ACTPATH+'/Index/lock_login',//这里是接收数据的PHP程序
						data:ajaxData,//传给PHP的数据，多个参数用&连接
						dataType:'text',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
						cache: false,        
						async: false,		
						success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
								rtnmsg=trim(msg);
						}
					});
					if (rtnmsg !== '1') {
						art.dialog({
							content: '登录信息错误！',
							icon: 'error',
							resize:false,
							max:false,
							min:false,
							lock: true,
							time: 1.5
						});
						username.focus();
						return false;
					} else {
						art.dialog({
							content: '恭喜你，回答正确！',
							icon: 'succeed',
							fixed: true,
							lock: true,
							time: 1.5
						});
						Sidebar.init({
							location:'right',//初始化sidebar的位置为左侧
							Icon:[dock.split(",")]
						});
						BottomBar.init();//初始化下部栏
						appManagerPanel.init();//初始化全局桌面
					};
				},
				cancel: false
			});			
					
		}, 
		login:function(){
			var dialog = art.dialog({
				lock: true,
				title:'系统登录',
				content: ''
					+ '<br>帐号：<input id="username" style=" padding:4px" /><br>'
					+ '<br>密码：<input id="password" style=" padding:4px" /><br>',
				fixed: true,
				resize:false,
				max:false,
				min:false,
				id: 'Fm7',
				icon: 'face-smile',
				okVal: '开始登录',
				ok: function () {
					var username = $("#username").val();
					var password = $("#password").val();
					ajaxData={
							username:username,
							password:password
						}
					$.ajax({
						type:'post',//可选
						url:ACTPATH+'/Index/lock_login',//这里是接收数据的PHP程序
						data:ajaxData,//传给PHP的数据，多个参数用&连接
						dataType:'text',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
						cache: false,        
						async: false,		
						success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
								rtnmsg=trim(msg);
						}
					});
					if (rtnmsg !== '1') {
						art.dialog({
							content: '登录信息错误！',
							icon: 'error',
							resize:false,
							max:false,
							min:false,
							lock: true,
							time: 1.5
						});
						username.focus();
						return false;
					} else {
						art.dialog({
							content: '恭喜你，回答正确！',
							icon: 'succeed',
							fixed: true,
							lock: true,
							time: 1.5
						});
						window.location.href=ACTPATH;
					};
				},
				cancel: true
			});			
			
		},
		logout:function(){		
			$.ajax({
				type:'post',//可选
				url:ACTPATH+'/Index/logout',//这里是接收数据的PHP程序
				data:'',//传给PHP的数据，多个参数用&连接
				dataType:'text',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
				cache: false,        
				async: false,		
				success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
					window.location.href=ACTPATH;
				}
			});
		}
	};
}();



//创建桌面最外层类
systemAjax=function(me){
	return me={	
		wallpaper:function(){
			/*id ='themSetting';
			title ='设置主题';
			url	=ACTPATH+'/Index/wallpaper';
			icon=UIPATH+"icon/config.png";
			width =650;
			height=500;
			Windows.openApp(id,title,url,icon,width,height);
*/
			$.ajax({
				type:'post',//可选
				url:ACTPATH+'/Index/wallpaper',//这里是接收数据的PHP程序
				data:'',//传给PHP的数据，多个参数用&连接
				dataType:'text',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
				cache: false,        
				async: false,		
				success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
					Windows.openSys({
						id :'themSetting',
						title :'设置主题',
						width :650,
						height:400,
						content :msg
					});	
				}
			});				
		},
		wallpaperSet:function(id){
				alert(id);			
		}
	};//end return 
}();



function wallpaper_set(imgsrc,id){
	$.ajax({
		type:'post',//可选
		url:ACTPATH+'/Index/wallpaper_set',//这里是接收数据的PHP程序
		data:'id='+id,//传给PHP的数据，多个参数用&连接
		dataType:'text',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
		cache: false,        
		async: false,		
		success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！			
			var src=UIPATH+trim(imgsrc);
			var h = $(window).height();
			var w = $(window).width();
			$("#zoomWallpaper").attr("src",src).width(w).height(h);
			$("#zoomWallpaperGrid").width(w).height(h);
		}
	});
}

function lock_login(){
	$.ajax({
		type:'post',//可选
		url:ACTPATH+'/Index/logout',//这里是接收数据的PHP程序
		data:'',//传给PHP的数据，多个参数用&连接
		dataType:'text',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
		cache: false,        
		async: false,		
		success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
		
		}
	});

	var dialog = art.dialog({
		lock: true,
		title:'系统登录',
		content: ''
			+ '<br>帐号：<input id="username" style=" padding:4px" /><br>'
			+ '<br>密码：<input id="password" style=" padding:4px" /><br>',
		fixed: true,
		resize:false,
		max:false,
		min:false,
		id: 'Fm7',
		icon: 'face-smile',
		okVal: '开始登录',
		ok: function () {
			var username = $("#username").val();
			var password = $("#password").val();
			ajaxData={
					username:username,
					password:password
				}
			$.ajax({
				type:'post',//可选
				url:ACTPATH+'/Index/lock_login',//这里是接收数据的PHP程序
				data:ajaxData,//传给PHP的数据，多个参数用&连接
				dataType:'text',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
				cache: false,        
				async: false,		
				success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
						rtnmsg=trim(msg);
				}
			});
			if (rtnmsg !== '1') {
				art.dialog({
					content: '登录信息错误！',
					icon: 'error',
					resize:false,
					max:false,
					min:false,
					lock: true,
					time: 1.5
				});
				username.focus();
				return false;
			} else {
				art.dialog({
					content: '恭喜你，回答正确！',
					icon: 'succeed',
					fixed: true,
					lock: true,
					time: 1.5
				});
				Sidebar.init({
					location:'right',//初始化sidebar的位置为左侧
					Icon:[dock.split(",")]
				});
				BottomBar.init();//初始化下部栏
				appManagerPanel.init();//初始化全局桌面
			};
		},
		cancel: true
	});
}

//function sAppData(){
//	$.ajax({
//		type:'post',//可选
//		url:'ajax.php',//这里是接收数据的PHP程序
//		data:'ac=sapp',//传给PHP的数据，多个参数用&连接
//		dataType:'text',
//        cache: false,        
//		async: false,		
//		contentType: "application/x-www-form-urlencoded; charset=utf-8",
//		success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
//				sAppData = eval('(' + msg + ')');
//			},
//		});	
//	return sAppData;
//}
//function appData(appid){
//	$.ajax({
//		type:'post',//可选
//		url:'ajax.php',//这里是接收数据的PHP程序
//		data:'ac=app&appid='+appid,//传给PHP的数据，多个参数用&连接
//		dataType:'text',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
//        cache: false,        
//		async: false,		
//		contentType: "application/x-www-form-urlencoded; charset=utf-8",
//		success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
//				appData = eval('(' + msg + ')');
//			},
//		});	
//	return appData;
//}
//
//
////return string ex: qq,jingshang,
//function deskIconData(desknum){
//	$.ajax({
//		type:'post',//可选
//		url:'ajax.php',//这里是接收数据的PHP程序
//		data:'ac='+desknum,//传给PHP的数据，多个参数用&连接
//		dataType:'text',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
//        cache: false,        
//		async: false,		
//		success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
//			//data = eval('(' + msg + ')');
//			txtStr=msg				
//		},
//		error: function(){
//			 txtStr=false;
//		},
//	});	
//	return txtStr;
//}