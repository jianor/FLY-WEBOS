<?php include("header.php"); ?>
<body>
	<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>权限管理-修改</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="<?php echo ACT?>/Role/role_modify/id/<?php echo $one["id"]?>" method="post" class="form-horizontal" />
									<div class="control-group">
										<label class="control-label">名称</label>
										<div class="controls">
											<input type="text" name="name" value="<?php echo $one["name"]?>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">权限</label>
										<div class="controls">
											<?php echo $app?>
										</div>
									</div>
							  
									<div class="control-group">
										<label class="control-label">描述</label>
										<div class="controls">
											<textarea name="remark"><?php echo $one["remark"]?></textarea>
										</div>
									</div>
									<div class="form-actions">
										<button type="submit" class="btn btn-primary">保存</button>
										<button type="button" class="btn btn-primary" onClick="javascript:window.history.go(-1);">返回</button>
									</div>
								</form>
							</div>
						</div>
</body>
</html>
