<?php 
include("../../global.php");
$result=$db->fetchAll("select * from wallpaper");
?>
<link href="../../css/main.css" rel="stylesheet" type="text/css" />
<link href="../../css/themesetting.css" rel="stylesheet"  type="text/css" >
<link href="../../css/skins/black.css" rel="stylesheet" />
<link href="../../css/powerFloat.css" rel="stylesheet" type="text/css"/>	
<link href="../../css/smartMenu.css" rel="stylesheet"  type="text/css" />
<script  type="text/javascript" src="../../js/jquery-1.7.1.min.js"></script>
<div id="themeSetting_wrap" >			
				<div id="themeSetting_head" class="themeSetting_head">		
					<div id="themeSetting_tabTheme" class="themeSetting_tab current" style="display: block;">系统主题</div>		
				</div>					
				<div id="themeSetting_body" class="themeSetting_body">	
					<div id="themeSetting_area"  class="themeSetting_area" style="display: block;">		
					<?php
						if(is_array($result)){
							foreach($result as $key => $rs){
					?>				 
					<a href="###" themeid="<?=$rs["id"]?>" class="themeSetting_settingButton" src="<?=$rs["url"]?>" id="themeSetting_theme_blue">
						<div style="background: url(../../<?=$rs["url"]?>) no-repeat;" class="themeSetting_settingButton_icon"></div>
						<div class="themeSetting_settingButton_text"><?=$rs["title"]?></div>
					</a>   
					<?php 
						}}
					?>          
					</div>						
					<div id="themeSetting_wallpaper" class="themeSetting_wallpaper" style="display: none;"></div>			
				</div>
		</div>	
<script>
 var themsSetting = $("#themeSetting_wrap");		
 //设置桌面背景的问题
 $("a",themsSetting).click(function(){
		var a  = $(this);
		var themeid = a.attr("themeid");
		//var src = themeid.substring(themeid.indexOf("_")+1,themeid.length);
		var src=a.attr("src");
		var h = $(window.parent).height();
		var w = $(window.parent).width();
		$("#zoomWallpaper",window.parent.document).attr("src",src).width(w).height(h);
		$("#zoomWallpaperGrid",window.parent.document).width(w).height(h);
		$("a",themsSetting).removeClass("themeSetting_selected");
		a.addClass("themeSetting_selected");
		$.ajax({
			type:'post',//可选
			url:'custom.ajax.php',//这里是接收数据的PHP程序
			data:'ac=modfiy&wallpaper_id='+themeid,//传给PHP的数据，多个参数用&连接
			dataType:'text',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
			cache: false,        
			async: false,		
			success:function(msg){//这里是ajax提交成功后，PHP程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
				
				},
			});	
 });
</script>